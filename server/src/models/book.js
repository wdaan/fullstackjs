const mongoose = require('mongoose')

const { Schema } = mongoose

const BookSchema = new Schema({
    title: String,
    author: String,
    type: { type: String, default: 'book' },
    createdAt: { type: Date, default: Date.now }
})

BookSchema.virtual('id').get(function () {
    return this._id.toHexString()
})

BookSchema.set('toJSON', {
    virtuals: true,
    transform: (doc, ret, options) => {
        delete ret.__v
        delete ret._id
    }
})

module.exports = mongoose.model('Book', BookSchema)
