const express = require('express')
const bodyParser = require('body-parser')
const Book = require('./models/book')

const app = express()

app.use(bodyParser.json())

app.get('/', (req, res) => {
    res.json({ msg: 'books' })
})

app.get('/api/v1/books', async (req, res) => {
    const books = await Book.find({}, {}, { sort: { createdAt: -1 } })
    return res.json(books)
})

app.get('/api/v1/books/:id', async (req, res) => {
    return res.json(await Book.findById(req.params.id))
})

app.delete('/api/v1/books/:id', async (req, res) => {
    return res.json(await Book.deleteOne({ _id: req.params.id }))
})

app.put('/api/v1/books/:id', async (req, res) => {
    const book = req.body.book
    delete book.id
    return res.json(
        await Book.findOneAndUpdate({ _id: req.params.id }, book, {
            returnOriginal: false
        })
    )
})

app.post('/api/v1/books', async (req, res) => {
    const book = req.body.book
    return res.json(
        await new Book({
            ...book
        }).save()
    )
})

module.exports = app
