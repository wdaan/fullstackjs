const mongoose = require('mongoose')
const app = require('./src/app')
const { DB_URI } = require('./src/config')

mongoose.connect(DB_URI, { useNewUrlParser: true, useUnifiedTopology: true })

const PORT = process.env.PORT || 3000

app.listen(PORT, () => {
    console.log(`🚀 Listening on port ${PORT}...`)
    console.log('--------------------------')
})
