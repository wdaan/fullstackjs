import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/Home.vue'

Vue.use(VueRouter)

const routes = [
    {
        path: '/',
        name: 'home',
        component: Home
    },
    {
        path: '/create',
        name: 'CreateBook',
        component: () => import(/* webpackChunkName: "CreateBook" */ '../views/CreateBook.vue')
    },
    {
        path: '/book/:bookId',
        name: 'BookDetail',
        component: () => import(/* webpackChunkName: "BookDetail" */ '../views/BookDetail.vue')
    },
    {
        path: '/book/:bookId/edit',
        name: 'EditBook',
        component: () => import(/* webpackChunkName: "EditBook" */ '../views/EditBook.vue')
    },
    {
        path: '*',
        redirect: '/'
    }
]

const router = new VueRouter({
    mode: 'history',
    base: process.env.BASE_URL,
    routes
})

export default router
