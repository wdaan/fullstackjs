import axios from 'axios'

class BookService {
    constructor() {
        this.axios = axios.create({
            baseURL: '/api/v1/books'
        })
    }

    getBooks() {
        return this.axios.get('/')
    }

    getBook(id) {
        return this.axios.get(`/${id}`)
    }

    deleteBook(id) {
        return this.axios.delete(`/${id}`)
    }

    createBook(book) {
        return this.axios.post('/', { book }).then(res => res.data)
    }

    updateBook(book) {
        return this.axios.put(`/${book.id}`, { book }).then(res => res.data)
    }
}

export default new BookService()
