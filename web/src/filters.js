import Vue from 'vue'

Vue.filter('formatTimeString', value => {
    if (!value) return ''
    const date = new Date(value)
    return `${date.toLocaleDateString()} - ${date.toLocaleTimeString()}`
})
