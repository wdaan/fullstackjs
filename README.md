# fullstackjs

## example repo for fullstack js development

### contains:

- node backend REST-api
- frontend (vue/vuetify)
- docker containers for every service (combined in docker-compose)
- linting & testing setup for each service
- pre-commit linting & testing hooks (husky)
- kubernetes deployment